#!/bin/bash

# ------------------------------------------------------------------------------------------ 
# Asterisk Backup with S3
# note: to push the files to Amazon S3 you need to install s3cmd from http://s3tools.org/s3cmd
# author: Alexander Ababii (alexcr@pbxware.ru)
# -
# ALL configs to config_s3.sh

source /backup/config_s3.sh

# -----------------------------------------------------------------------------------------
# get the current space on the box..
echo "Making directory "$BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY
mkdir $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY

# create the report 
touch $REPORT_LOCATION

echo $REPORT_NEWLINE | tee -a $REPORT_LOCATION
echo "Backup Report ("$REPORT_LOCATION") on `date +%m-%d-%Y`" | tee -a $REPORT_LOCATION

tar --exclude='*.wav' --exclude='*.mp3' --exclude='*.WAV' --exclude='*.gsm' -cvf $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY"/etc.tar.gz" $ETC_LOCATION
tar --exclude='*.wav' --exclude='*.mp3' --exclude='*.WAV' --exclude='*.gsm' -cvf $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY"/modules.tar.gz" $MODULES_LOCATION
tar --exclude='*.wav' --exclude='*.mp3' --exclude='*.WAV' --exclude='*.gsm' -cvf $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY"/lib.tar.gz" $LIB_LOCATION
tar --exclude='*.wav' --exclude='*.mp3' --exclude='*.WAV' --exclude='*.gsm' -cvf $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY"/spool.tar.gz" $SPOOL_LOCATION
tar --exclude='*.wav' --exclude='*.mp3' --exclude='*.WAV' --exclude='*.gsm' -cvf $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY"/WEB.tar.gz" $WEB_LOCATION

echo $REPORT_NEWLINE | tee -a $REPORT_LOCATION
echo "Backup Report ("$REPORT_LOCATION") on `date +%m-%d-%Y`" | tee -a $REPORT_LOCATION

#Wget autosupport script and run it
if [ -f /usr/sbin/autosupport ]; then
echo "File exist"
wget http://svn.asterisk.org/svn/asterisk/trunk/contrib/scripts/autosupport -O /usr/sbin/autosupport
chmod +x /usr/sbin/autosupport
else
echo "File not found"
autosupport -n backup
mv /root/digium-info* $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY/
fi


echo $REPORT_NEWLINE | tee -a $REPORT_LOCATION
ls -al $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY | tee -a $REPORT_LOCATION
# get the current space on the box..
df -h | tee -a $REPORT_LOCATION

# backup MySQL
echo $REPORT_NEWLINE | tee -a $REPORT_LOCATION
echo "Dumping the following MySQL databases :: '"$DATABASE_BACKUP_LIST"' into :: "$MYSQLDUMP_LOCATION | tee -a $REPORT_LOCATION
mysqldump --user=$MYSQL_USERNAME --password=$MYSQL_PASSWORD --all-databases $DATABASE_BACKUP_LIST | gzip > $MYSQLDUMP_LOCATION

# zip up the tape archive 
#echo $REPORT_NEWLINE | tee -a $REPORT_LOCATION
#echo "Zip up the filesystem :: "$BACKUP_DESTINATION | tee -a $REPORT_LOCATION

#zip -r --password pbxware $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY.zip $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY

# now, push everything over to Amazon S3
echo $REPORT_NEWLINE | tee -a $REPORT_LOCATION

echo "Sending the backup to Amazon.. " | tee -a $REPORT_LOCATION
echo " /usr/local/bin/aws s3 sync $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY s3://$BUCKED/$ASTERNAME --region eu-central-1"

/usr/local/bin/aws s3 sync $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY s3://$BUCKED/$ASTERNAME/ --region eu-central-1 | tee -a $REPORT_LOCATION

#rm dir
#echo "Remove dir $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY .. " | tee -a $REPORT_LOCATION
#rm -fr $BACKUP_DIRECTORY$TODAYS_BACKUP_DIRECTORY

#remove old backup files 10 day 
#echo "find $BACKUP_DIRECTORY/*.zip -mtime +10 -exec rm {} \;" | tee -a $REPORT_LOCATION
#find $BACKUP_DIRECTORY*.zip -mtime +10 -exec rm {} \;

# send the reporting email..
#mail -s $EMAIL_SUBJECT $EMAIL_RECIPIENT < $REPORT_LOCATION

#echo "(REPORT) vi " $REPORT_LOCATION

# gunzip source_1292789551.tar.gz
# tar -xvf source_1292789551.tar
