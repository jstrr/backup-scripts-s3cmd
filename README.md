# README #

s3cmd for aws cli

after adding new region in s3 you'll get an error: A client error (InvalidRequest) occurred when calling the ListObjects operation: The authorization mechanism you have provided is not supported. Please use AWS4-HMAC-SHA256.
This error is caused by changing authorization mechanism, so you can't use s3cmd any more. Instead of this you should use awscli.
For Ubuntu you can't install it using aptitude, 'cause of awxcli and python version. It should be compiled from sources:
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" unzip awscli-bundle.zip sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
If you don't have unzip, use your Linux distribution's built in package manager to install it, for Ubuntu it is sudo aptitude install unzip.
Don’t forget to copy aws credentials from ~/.aws -R to /root (necessary for cron)

aws configure - for keys


